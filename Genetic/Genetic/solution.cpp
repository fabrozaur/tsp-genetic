#include <vector>
#include <memory>
#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include "solution.h"
#include "parser.h"

using namespace std;

solution::solution(int number, float pc, float pm, int mutation_type)
{
	this->number = number;
	this->Pc = pc;
	this->Pm = pm;
	this->range = range;
	this->mutation_type = mutation_type;
	srand(time(NULL));
	specimen = make_unique <vector <vector <int>>>(number);

	for (int i = 0; i < number; i++)
		(*specimen)[i].emplace_back();

	random();
	specimensort();
}

void solution::inversion(int index)
{
	if (rand() % 100 <= Pm * 100)
	{
		int x2, x1 = rand() % parser::dimension;
		do {
			x2 = rand() % parser::dimension;
		} while (x1 == x2);
		if (x1 > x2)
			reverse(specimen->at(index).begin() + x2, specimen->at(index).begin() + x1);
		else
			reverse(specimen->at(index).begin() + x1, specimen->at(index).begin() + x2);
	}
	return;
}

void solution::specimensort()
{
	sort(specimen->begin(), specimen->end(), [&](vector<int>& a, vector<int>& b) {
		return getpathcost(a) < getpathcost(b);
	});

}

int solution::tournament(int size)
{
	unique_ptr <vector<int>> v = make_unique <vector<int>>(), result = make_unique <vector<int>>();
	for (int i = 0; i < number; i++)
	{
		v->push_back(i);
	}
	int temp;
	for (int j = 0; j < size; j++)
	{
		temp = rand() % v->size();
		result->push_back((*v)[temp]);
		v->erase(v->begin() + temp);
	}
	sort(result->begin(), result->end(), [&](int& a, int& b) {
		return getpathcost(a) < getpathcost(b);
	});
	return (*result)[0];
}

void solution::scramble(int index)
{
	if (rand() % 100 <= Pm * 100)
	{
		int count;
		do {
			count = rand() % parser::dimension;
		} while (count < 2);
		unique_ptr <vector<int>> chosen = make_unique <vector<int>>();
		unique_ptr <vector<int>> v = make_unique <vector<int>>();
		for (int i = 0; i < parser::dimension; i++)
		{
			v->push_back(i);
		}
		int temp;
		for (int i = 0; i < count; i++)
		{
			temp = rand() % v->size();
			chosen->push_back(v->at(temp));
			v->erase(v->begin() + temp);
		}
		temp = (*specimen)[index][chosen->at(0)];
		for (int i = 0; i < chosen->size()-1; i++)
		{
			(*specimen)[index][chosen->at(i)] = (*specimen)[index][chosen->at(i + 1)];
		}
		(*specimen)[index][chosen->at(chosen->size()-1)] = temp;
	}
	
	return;
}

void solution::PMX(int id1, int id2)
{
		int length1 = rand() % parser::dimension, length2;
		do {
			length2 = rand() % parser::dimension;
		} while (length1 == length2 || (length1 == 0 && length2 == parser::dimension - 1) || (length2 == 0 && length1 == parser::dimension - 1));
		if (length1 > length2)
		{
			swap(length1, length2);
		}

		unique_ptr <vector <int>> child1 = make_unique <vector<int>>(parser::dimension);

		for (int i = 0; i < parser::dimension; i++)
		{
			(*child1)[i] = -1;
		}

		copy((*specimen)[id1].begin() + length1, (*specimen)[id1].begin() + length2 + 1, child1->begin() + length1);

		for (int i = length1; i <= length2; i++)
		{
			auto p = find(specimen->at(id1).begin(), specimen->at(id1).end(), (*specimen)[id2][i]);
			bool end = false;
			for (int j = length1; j <= length2; j++)
			{
				if (p == specimen->at(id1).begin() + j)
				{
					end = true;
				}
			}

			if (end == false)
			{
				auto r = find(specimen->at(id2).begin(), specimen->at(id2).end(), (*specimen)[id1][i]);
				end = true;

				for (int j = length1; j <= length2; j++)
				{
					if (r == specimen->at(id2).begin() + j)
					{
						end = false;
					}
				}

				if (end == true)
				{
					if (r != specimen->at(id2).end())
						(*child1)[(r - specimen->at(id2).begin())] = (*specimen)[id2][i];
					else
						cout << "error";
				}
				else
				{
					while (true) {
						for (int j = 0; j < parser::dimension; j++)
						{
							if ((*specimen)[id2][j] == (*specimen)[id1][r - specimen->at(id2).begin()])
							{
								r = specimen->at(id2).begin() + j;
								break;
							}
						}
						bool isinrange = false;
						for (int j = length1; j <= length2; j++)
						{
							if (*r == (*specimen)[id2][j])
								isinrange = true;
						}

						if (isinrange == false)
						{
							(*child1)[r - specimen->at(id2).begin()] = (*specimen)[id2][i];
							break;
						}
					}
				}
			}
		}
		for (int i = 0; i < child1->size(); i++)
		{
			if ((*child1)[i] == -1)
			{
				(*child1)[i] = (*specimen)[id2][i];
			}
		}

		specimen->push_back(*child1);

		if (mutation_type == 1)
			scramble(specimen->size() - 1);
		else
			inversion(specimen->size() - 1);
		
		specimensort();

		specimen->erase(specimen->begin() + specimen->size() - 1);

	
	return;
}

void solution::setpc(int param)
{
	this->Pc = param;
}

void solution::setpm(int param)
{
	this->Pm = param;
}

void solution::OX(int id1, int id2)
{
		int length1 = rand() % parser::dimension, length2;
		do {
			length2 = rand() % parser::dimension;
		} while (length1 == length2 || (length1 == 0 && length2 == parser::dimension - 1) || (length2 == 0 && length1 == parser::dimension - 1));
		if (length1 > length2)
		{
			swap(length1, length2);
		}

		unique_ptr <vector <int>> child1 = make_unique <vector<int>>(length2 - length1 + 1);

		copy((*specimen)[id1].begin() + length1, (*specimen)[id1].begin() + length2 + 1, child1->begin());

		for (int i = length2 + 1; i < parser::dimension; i++)
		{
			auto p = find(child1->begin(), child1->end(), (*specimen)[id2][i]);
			if (p == child1->end())
				child1->push_back((*specimen)[id2][i]);
		}
		if (child1->size() < parser::dimension)
		{
			int i = 0, j = 0;
			while (child1->size() != parser::dimension)
			{
				auto p = find(child1->begin(), child1->end(), (*specimen)[id2][i]);
				if (p == child1->end())
				{
					if (child1->size() < parser::dimension - length1)
					{
						child1->push_back((*specimen)[id2][i]);
					}
					else
					{
						child1->insert(child1->begin() + j, (*specimen)[id2][i]);
						j++;
					}
				}
				i++;
			}
		}

		specimen->push_back(*child1);
		if (mutation_type == 1)
			scramble(specimen->size() - 1);
		else
			inversion(specimen->size() - 1);

		specimensort();
		specimen->erase(specimen->begin() + specimen->size() - 1);	
	
	return;
}

int solution::getpathcost(int x)
{
	int cost=0;
	for (int i = 0; i < parser::dimension-1; i++)
	{
		cost += parser::weight[(*specimen)[x][i]][(*specimen)[x][i+1]];
	}
	cost += parser::weight[(*specimen)[x][parser::dimension-1]][(*specimen)[x][0]];
	return cost;
}

int solution::getpathcost(vector<int> &colection) const
{
	int cost = 0;
	for (int i = 0; i < parser::dimension-1; i++)
	{
		cost += parser::weight[colection[i]][colection[i+1]];
	}
	cost += parser::weight[colection[colection.size() - 1]][colection[0]];
	return cost;
}

void solution::getspecimen(int row)
{
	for (int i = 0; i < parser::dimension; i++)
	{
		cout << (*specimen)[row][i] <<" ";
	}
	cout << endl;
}

void solution::showspecimens()
{
	for (int i = 0; i < number; i++)
	{
		for (int j = 0; j < parser::dimension; j++)
		{
			cout << (*specimen)[i][j] << " ";
		}
		cout << "Koszt:" << getpathcost((*specimen)[i]) << endl;
	}
	cout << endl;
}

void solution::random() 
{
	unique_ptr <vector<int>> v = make_unique <vector<int>>();
	for (int i = 0; i < number; i++)
	{
			for (int j = 0; j < parser::dimension; j++)
			{
				v->push_back(j);
			}
			int temp;
			(*specimen)[i].clear();
			for (int k = 0; k < parser::dimension; k++)
			{
				temp = rand() % v->size();
				(*specimen)[i].push_back((*v)[temp]);
				v->erase(v->begin() + temp);
			}	
	}
	
}

solution::~solution()
{
}
