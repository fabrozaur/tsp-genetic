#pragma once
#include <vector>
#include <memory>
using namespace std;

class solution
{
public:
	solution(int solution_size, float pc, float pm, int mutation_type);
	~solution();
	void showspecimens();
	void getspecimen(int);
	int getpathcost(int);
	int getpathcost(vector<int>&) const;
	//Mutations
	void inversion(int);
	void scramble(int);
	//Crossing operators
	void OX(int parent1, int parent2);
	void PMX(int parent1, int parent2);
	//Parents selection
	int tournament(int size);
	//geters, seters
	void setpc(int);
	void setpm(int);
private:
	unique_ptr <vector <vector<int>>> specimen;
	int number, mutation_type;
	float Pc, Pm, range;
	void random();
	void specimensort();
	
};

