#include <iostream>
#include <string>
#include "parser.h"
#include "tinyxml.h"
#include "tinystr.h"
using namespace std;
parser::parser()
{
	
}

double** parser::weight;
int parser::dimension;

void parser::fill()
{
	weight = new double*[dimension];
	
	for (int i = 0; i < dimension; i++)
	{
		weight[i] = new double[dimension];
		for (int j = 0; j < dimension; j++)
		{
			weight[i][j] = 0;
		}
	}
}

void parser::asymetric(string name)
{
	
	TiXmlDocument doc(name.c_str());
	doc.LoadFile();
	
	TiXmlElement* root;

	root = doc.FirstChildElement();
	root = root->FirstChildElement("graph");
	root = root->FirstChildElement();
	TiXmlElement *temp = root->FirstChildElement();
	for (; temp->NextSiblingElement() != NULL; temp = temp->NextSiblingElement())
	{
	}
	dimension = stoi(temp->GetText()) + 1;
	fill();
	float val;
	for (int i = 0; i < dimension; i++)
	{
		TiXmlElement *edge = root->FirstChildElement();

		for (int j = 0; j < dimension; j++)
		{
				edge->QueryFloatAttribute("cost", &val);
				weight[i][j] = val;
				//cout << edge->Value() << " text: " << edge->GetText() << " value: " << val << endl;
				edge = edge->NextSiblingElement();
		}
		root = root->NextSiblingElement();
	}
}

void parser::symetric(string name)
{

	TiXmlDocument doc(name.c_str());
	doc.LoadFile();
	TiXmlElement* root;

	root = doc.FirstChildElement();
	root = root->FirstChildElement("graph");
	root = root->FirstChildElement();
	TiXmlElement *temp = root->FirstChildElement();
	for (; temp->NextSiblingElement() != NULL; temp = temp->NextSiblingElement())
	{
	}
	dimension = stoi(temp->GetText()) + 1;
	fill();
	float val;
	for (int i = 0; i < dimension; i++)
	{
		TiXmlElement *edge = root->FirstChildElement();

		for (int j = 0; j < dimension; j++)
		{
			if (i != j)
			{
				edge->QueryFloatAttribute("cost", &val);
				weight[i][j] = val;
				//cout << edge->Value() << " text: " << edge->GetText() << " value: " << val << endl;
				edge = edge->NextSiblingElement();
			}
		}
		root = root->NextSiblingElement();
	}
}
parser::~parser()
{
}
