#include <iostream>
#include <ctime>
#include <vector>
#include "parser.h"
#include "solution.h"

using namespace std;

void read_data()
{
	parser *ob = new parser();
	string dane;
	bool sym;
	cout << "Podaj sciezke pliku xml" << endl;
	cin >> dane;
	cout << "Czy dane sa symetryczne? (1 - tak, 0 - nie)" << endl;
	cin >> sym;
	if (!sym)
		ob->asymetric(dane);
	else
		ob->symetric(dane);
	return;
}

void run_program_menu()
{
	float fparam[2];
	int iparam[2];
	time_t t;
	double sum = 0;

	cout << "Podaj wielkosc populacji" << endl;
	cin >> iparam[0];

	cout << "Podaj parametr Pc" << endl;
	cin >> fparam[0];
	cout << "Podaj parametr Pm" << endl;
	cin >> fparam[1];

	cout << "Jakich mutacji chcesz uzywac?" << endl;
	cout << "1. scramble" << endl;
	cout << "2. inverse" << endl;
	cin >> iparam[1];

	solution *sol = new solution(iparam[0], fparam[0], fparam[1], iparam[1]);

	cout << endl << "Jakich krzyzowan chcesz uzywac?" << endl;
	cout << "1. PMX" << endl;
	cout << "2. OX" << endl;
	cin >> iparam[0];

	cout << "Podaj ilosc iteracji programu" << endl;
	cin >> iparam[1];

	cout << "Podaj wielkosc turnieju" << endl;
	cin >> iparam[2];
	int parent1 = sol->tournament(iparam[2]), parent2 = sol->tournament(iparam[2]);

	if (iparam[0] == 1)
	{

		t = clock();
		for (int i = 0; i < iparam[1]; i++)
		{
			sol->PMX(parent1, parent2);
			sol->PMX(parent2, parent1);
		}
		sum += clock() - t;
	}
	else
	{
		t = clock();
		for (int i = 0; i < iparam[1]; i++)
		{
			sol->OX(parent1, parent2);
			sol->OX(parent2, parent1);
		}
		sum += clock() - t;
	}

	cout << endl << endl;
	cout << "Wektor rozwiazan to: ";
	sol->getspecimen(0);
	cout << "Koszt przejscia po grafie to: " << sol->getpathcost(0) << endl;
	cout << "Czas operacji to: " << sum / CLOCKS_PER_SEC << endl;
}

int main()
{
	read_data();
	run_program_menu();
	system("pause");
	return 0;
}