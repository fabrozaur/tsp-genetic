#pragma once
#include <iostream>
#include <string>
#include "parser.h"
#include "tinyxml.h"
#include "tinystr.h"

using namespace std;

class parser
{
private:

	void fill();
public:
	parser();
	friend class solution;

	static int dimension;
	static double** weight;
	
	void symetric(string);
	void asymetric(string);
	

	~parser();
};

